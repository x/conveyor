============
Installation
============

At the command line::

    $ pip install conveyor

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv conveyor
    $ pip install conveyor
